jQuery(document).ready(function($) {
  jQuery( "#role" ).change(function() {
    var role = jQuery( "#role" ).val();
    var data = {
      'action': 'role_change',
      'role': role,
    };
    jQuery.post(ajax_object.ajax_url, data, function(response) {
      var obj = jQuery.parseJSON(response);
      jQuery.each(obj, function( key, value ){
        if(true==value){
          jQuery("#"+key).prop("checked",true);
        }else{
          jQuery("#"+key).prop("checked",false);
        }
      });
    });
  });
});
