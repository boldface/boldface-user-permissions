<?php
/**
 * Methods/admin-enqueue-scripts.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for enqueueing scripts
 */
class admin_enqueue_scripts extends methods\admin_enqueue {

  /**
   * @var string The js directory
   *
   * @access protected
   * @since 0.1
   */
  protected $js_dir  = '/boldface-user-permissions/js/';

  /**
   * @var array The scripts to be enqueued
   *
   * @access protected
   * @since 0.1
   */
  protected $scripts;

  /**
   * Object constructor
   *
   * @param array $scripts Array of scripts to enqueue
   *
   * @access public
   * @since 0.1
   */
  public function __construct( array $scripts = [] ) {
    $this->scripts = $scripts;
  }

  /**
   * Enqueue the scripts
   *
   * @access public
   * @since 0.1
   */
  public function enqueue_scripts() {
    foreach( $this->scripts as $script => $args ) {
      \wp_enqueue_script(
        $this->slug( $script ),
        $this->js_url( $script ),
        isset( $args[ 'deps' ] ) ? $args[ 'deps' ] : [],
        isset( $args[ 'ver' ] )  ? $args[ 'ver' ]  : ''
      );
      if( isset( $args[ 'localize' ] ) ) {
        \wp_localize_script(
          $this->slug( $script ),
          'ajax_object',
          [ 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 'editor' ]
        );
      }
    }
  }

  /**
   * Return the url for the script
   *
   * @param string $script The script
   *
   * @access protected
   * @since 0.1
   *
   * @return string The url for the script
   */
  protected function js_url( $script ) {
    return \plugins_url( $this->js_dir . $script . '.js' );
  }
}
