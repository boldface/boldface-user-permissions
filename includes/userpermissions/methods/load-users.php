<?php
/**
 * Methods/load-users.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for modifying the load-users.php page
 */
class load_users {

  /**
   * Add capabilities to the users columns
   *
   * @param array $column Array of columns to display on the load-users page
   *
   * @access public
   * @since 0.1
   *
   * @return array Array of columns
   */
  public function manage_users_columns( $column ) {
    $column[ 'capabilities' ] = __( 'Capabilities', 'boldface-user-permissions' );
    return $column;
  }

  /**
   * Add the number of capabilities to the custom user column
   *
   * @param array  $column       The column
   * @param string $column_name The column name
   * @param int    $user_id     The user ID
   *
   * @access public
   * @since 0.1
   *
   * @return string Number of capabilities
   */
  public function manage_users_custom_column( $column, $column_name, $user_id ) {
    $user = \get_user_by( 'id', $user_id );
    $caps = $this->user_caps( $user );
    return $this->user_caps_html( $caps );
  }

  /**
   * Return the user capabilities
   *
   * @param \WP_User $user WordPress WP_User object
   *
   * @access protected
   * @since 0.1
   *
   * @return array Array of capabilities
   */
  protected function user_caps( $user ) {
    $caps = [];
    foreach( $user->caps as $cap => $status ) {
      if( true !== $status || false !== strpos( $cap, 'level_' ) ) {
        unset( $user->caps[ $cap ] );
        continue;
      }
      $caps[] = $cap;
    }
    return $caps;
  }

  /**
   * Return formatted number of capabilities
   *
   * @param array $caps Array of capabilities
   *
   * @access protected
   * @since 0.1
   *
   * @return string HTML string
   */
  protected function user_caps_html( $caps ) {
    return sprintf( '<a href="">%1$s</a>', (string) count( $caps ) );
  }
}
