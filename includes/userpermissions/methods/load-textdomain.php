<?php
/**
* Methods/load-textdomain.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\Hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for loading the text domain
 */
class load_textdomain {

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Load the text domain
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \load_plugin_textdomain( 'boldface-user-permissions', false,
      dirname( \plugin_basename( $this->file ) ) . '/languages' );
  }
}
