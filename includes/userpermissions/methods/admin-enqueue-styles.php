<?php
/**
 * Methods/admin-enqueue-styles.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for enqueueing styles
 */
class admin_enqueue_styles extends methods\admin_enqueue {

  /**
   * @var string The css directory
   *
   * @access protected
   * @since 0.1
   */
  protected $css_dir  = '/boldface-user-permissions/css/';

  /**
   * @var array The styles to be enqueued
   *
   * @access protected
   * @since 0.1
   */
  protected $styles;

  /**
   * Object constructor
   *
   * @param array $styles Array of styles to enqueue
   *
   * @access public
   * @since 0.1
   */
  public function __construct( array $styles = [] ) {
    $this->styles = $styles;
  }

  /**
   * Enqueue the styles
   *
   * @access public
   * @since 0.1
   */
  public function enqueue_styles() {
    foreach( $this->styles as $style => $args ) {
      \wp_enqueue_style(
        $this->slug( $style ),
        $this->css_url( $style ),
        isset( $args[ 'deps' ] ) ? $args[ 'deps' ] : [],
        isset( $args[ 'ver' ] )  ? $args[ 'ver' ]  : ''
      );
    }
  }

  /**
   * Return the url for the style
   *
   * @param string $style The style
   *
   * @access protected
   * @since 0.1
   *
   * @return string The url for the style
   */
  protected function css_url( $style ) {
    return \plugins_url( $this->css_dir . $style . '.css' );
  }
}
