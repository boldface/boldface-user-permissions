<?php
/**
 * Methods/ajax.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for enqueueing scripts
 */
class ajax {

  /**
   * Ajax response when the role has changed on the user edit screen
   *
   * @access public
   * @since 0.1
   */
  public function role_change() {
    $role = \sanitize_text_field( $_POST[ 'role' ] );
    $caps = new \Boldface\UserPermissions\methods\capabilities();
    echo json_encode( $caps->from_role( $role ) );
    wp_die();
  }
}
