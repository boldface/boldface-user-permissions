<?php
/**
 * Methods/load-user-edit-update.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class load_user_edit_update {

  /**
   * @var array Sanitized $_POST array
   *
   * @access protected
   * @since 0.1
   */
  protected $post;

  /**
   * @var array Array of capabilities for the user
   *
   * @access protected
   * @since 0.1
   */
  protected $capabilities;

  /**
   * @var array Array of all the capabilities
   *
   * @access protected
   * @since 0.1
   */
  protected $allcaps;

  /**
   * @var \WP_User The WordPress user object
   *
   * @access protected
   * @since 0.1
   */
  protected $user;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->allcaps = ( new methods\capabilities() )->all_caps();
    $this->capabilities = [];
    $this->set_post();
    $this->set_role();
  }

  /**
   * Update the user capabilities
   *
   * @param int $user_id The user ID
   *
   * @access public
   * @since 0.1
   */
  public function user_edit_update( $user_id ) {
    if( ! \current_user_can( 'promote_users' ) ) {
      \wp_die( __( "You can't do that!", 'boldface-user-permissions' ) );
    }
    $this->user = new \WP_User( $user_id );
    $this->populate_capabilities();
    $this->add_caps();
  }

  /**
   * Return whether the $_POST variable is set and not empty
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the $_POST variable is set and not empty
   */
  protected function is_post() {
    return isset( $_POST ) && ! empty( $_POST );
  }

  /**
   * Check if $_POST and sanitize it
   *
   * @access protected
   * @since 0.1
   */
  protected function set_post() {
    $this->is_post() ? $this->sanitize_post() : null;
  }

  /**
   * Return whether the role variable is set
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the role variable is set
   */
  protected function is_role() {
    return isset( $this->post ) && isset( $this->post[ 'role' ] ) ? true : false;
  }

  /**
   * Sanitize and get role
   *
   * @access protected
   * @since 0.1
   */
  protected function set_role() {
    $this->rolename = isset( $_POST ) && isset( $_POST[ 'role' ] ) ?
      \sanitize_text_field( $_POST[ 'role' ] ) : null;
    $this->role = isset( $this->rolename ) ? \get_role( $this->rolename ) : null;
  }

  /**
   * Sanitize the $_POST
   *
   * @access protected
   * @since 0.1
   */
  protected function sanitize_post() {
    foreach( $this->allcaps as $cap ) {
      $this->post_set_cap( $cap );
    }
  }

  /**
   * Set capabilities from $_POST
   *
   * @access protected
   * @since 0.1
   */
  protected function post_set_cap( $cap ) {
    $this->post[ $cap ] = isset( $_POST[ $cap ] ) && ! empty( $_POST[ $cap ] ) ?
      true: false;
  }

  /**
   * Add capabilities to the user
   *
   * @access protected
   * @since 0.1
   */
  protected function add_caps() {
    foreach( $this->allcaps as $cap ) {
      if( $this->has_cap( $cap ) ) {
        $this->user->add_cap( $cap );
        if( ! in_array( $cap , $this->user->caps ) ) {
          $this->user->add_cap( $cap, $this->has_cap( $cap ) );
        }
      }
      else {
        $this->user->remove_cap( $cap );
        if( in_array( $cap , $this->user->caps ) ) {
          $this->user->add_cap( $cap, $this->has_cap( $cap ) );
        }
      }
    }
  }

  /**
   * Return whether the capabilities property has a certain capability
   *
   * @param string $cap Capability to check
   *
   * @access protected
   * @since 0.1
   *
   * @return Whether the capabilities property contains $cap
   */
  protected function has_cap( $cap ) {
    return isset( $this->capabilities[ $cap ] ) &&
      true === $this->capabilities[ $cap ] ? true : false;
  }

  /**
   * Populate the capabilities property
   *
   * @access protected
   * @since 0.1
   */
  protected function populate_capabilities() {
    //* If the role changed, override checked capabilities
    $this->role_changed() ?
      $this->populate_capabilities_from_role() :
      $this->populate_capabilities_from_post();
  }

  /**
   * Populate the capabilities property from the post
   *
   * @access protected
   * @since 0.1
   */
  protected function populate_capabilities_from_post() {
    foreach( $this->allcaps as $cap ) {
      $bool = isset( $_POST[ $cap ] ) && $this->post[ $cap ] ? true : false;
      $this->capabilities[ $cap ] = $bool;
    }
  }

  /**
   * Return whether the role has changed
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the role has changed
   */
  protected function role_changed() {
    return null === $this->rolename ? false :
      ! in_array( $this->rolename, $this->user->roles );
  }

  /**
   * Populate the capabilities property from the role
   *
   * @access protected
   * @since 0.1
   */
  protected function populate_capabilities_from_role() {
    $this->capabilities = $this->role->capabilities;
  }
}
