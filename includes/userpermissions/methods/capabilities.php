<?php
/**
 * Methods/capabilities.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with WordPress capabilities
 */
class capabilities {

  /**
   * @var array All the capabilities
   *
   * @access protected
   * @since 0.1
   */
  protected $allcaps;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->allcaps = [];
  }

  /**
   * Return all current capabilities used by roles
   *
   * @access public
   * @since 0.1
   *
   * @return array Array of capabilities
   */
  public function all_caps() {
    $this->caps_from_roles();
    $this->caps_from_users();
    sort( $this->allcaps );
    return $this->allcaps;
  }

  /**
   * Return the roles associated with a rolename
   *
   * @param string $rolename The rolename
   *
   * @access public
   * @since 0.1
   */
  public function from_role( $rolename ) {
    $all = array_fill_keys( $this->all_caps(), false );
    $role = get_role( $rolename );
    return array_merge( $all, $role->capabilities );
  }

  /**
   * Get the capabilities from all the roles
   *
   * @access protected
   * @since 0.1
   */
  protected function caps_from_roles() {
    global $wp_roles;
    foreach( $wp_roles->role_names as $role => $name ){
      foreach( $wp_roles->roles[ $role ][ 'capabilities' ] as $cap => $perm ) {
        if( ! $this->in_array( $cap ) && ! $this->is_exception( $cap ) ) {
          $this->add_to_array( $cap );
        }
      }
    }
  }

  /**
   * Get the capabilities from all the users
   *
   * @access protected
   * @since 0.1
   */
  protected function caps_from_users() {
    $users = \get_users();
    foreach( $users as $user ) {
      foreach( $user->caps as $cap => $perm ) {
        if( ! $this->in_array( $cap ) && ! $this->is_exception( $cap ) ) {
          $this->add_to_array( $cap );
        }
      }
    }
  }

  /**
   * Return whether the capability is in the array
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the capability is in the array
   */
  protected function in_array( $cap ) {
    return in_array( $cap, $this->allcaps );
  }

  /**
   * Add a capability to the capabilities array
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   */
  protected function add_to_array( $cap ) {
    $this->allcaps[] = $cap;
  }

  /**
   * Return whether this is an exception to the rule
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether this is an exception to the rule
   */
  protected function is_exception( $cap ) {
    if( $this->is_level( $cap ) && ! $this->levels() ) {
      return true;
    }
    if( $this->is_unfiltered_upload( $cap ) && $this->unfiltered_upload() ) {
      return true;
    }
    if( $this->is_manage_links( $cap ) && $this->manage_links() ) {
      return true;
    }
    return false;
  }

  /**
   * Return whether the capability is a level
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the capability is a level
   */
  protected function is_level( $cap ) {
    return false === stripos( $cap, 'level_' ) ? false : true;
  }

  /**
   * Return whether the ALLOW_USER_LEVELS constant is true
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the ALLOW_USER_LEVELS constant is true
   */
  protected function levels() {
    return defined( 'ALLOW_USER_LEVELS' ) ? true : false;
  }

  /**
   * Return whether the capability is unfiltered_upload
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the capability is unfiltered_upload
   */
  protected function is_unfiltered_upload( $cap ) {
    return 'unfiltered_upload' === $cap ? true : false;
  }

  /**
   * Return whether the ALLOW_UNFILTERED_UPLOAD constant is defined
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the ALLOW_UNFILTERED_UPLOAD constant is defined
   */
  protected function unfiltered_upload() {
    return defined( 'ALLOW_UNFILTERED_UPLOAD' ) ? true : false;
  }

  /**
   * Return whether the capability is manage_links
   *
   * @param string Capability
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the capability is manage_links
   */
  protected function is_manage_links( $cap ) {
    return 'manage_links' === $cap ? $this->manage_links() : __return_false();
  }

  /**
   * Return whether the ALLOW_MANAGE_LINKS constant is defined
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the ALLOW_MANAGE_LINKS constant is defined
   */
  protected function manage_links() {
    return defined( 'ALLOW_MANAGE_LINKS' ) ? false : true;
  }
}
