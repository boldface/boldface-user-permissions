<?php
/**
 * Methods/admin-enqueue.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for enqueueing styles
 */
abstract class admin_enqueue {

  /**
   * @var string The css slug
   *
   * @access protected
   * @since 0.1
   */
  protected $slug = 'boldface-user-permissions';

  /**
   * @var string The js directory
   *
   * @access protected
   * @since 0.1
   */
  protected $dir  = '/boldface-user-permissions/';

  /**
   * Return the slug
   *
   * @param string $slug String to add to slug
   *
   * @access protected
   * @since 0.1
   *
   * @return string The slug
   */
  protected function slug( $slug ) {
    return $this->slug . '-' . $slug;
  }
}
