<?php
/**
 * Methods/load-user-edit.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Methods;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class load_user_edit {

  /**
   * Output html on the edit user page
   *
   * @access public
   * @since 0.1
   */
  public function edit_user_profile() {
    $this->header();
    $this->list_caps();
  }

  /**
   * Print html header
   *
   * @access public
   * @since 0.1
   */
  protected function header() {
    printf( '<h2>Edit User Permissions</h2>' );
  }

  /**
   * Print html capabilities
   *
   * @access public
   * @since 0.1
   */
  protected function list_caps() {
    global $user_id;
    printf( '<ul class="flex">' );
    foreach( ( new methods\capabilities() )->all_caps() as $cap ) {
      if( 'unfiltered_upload' === $cap && ! defined( 'ALLOW_UNFILTERED_UPLOAD' ) ) {
        continue;
      }
      printf( '<li>' );
      $checked = \user_can( $user_id, $cap ) ? 'checked="checked"' : '';
      $this->checkbox( $cap, $checked );
      printf( '</li>' );
    }
    printf( '</ul>' );
  }

  /**
   * Print html checkbox
   *
   * @param string $cap     The capablitiy
   * @param string $checked Whether the checkbox should be checked
   *
   * @access public
   * @since 0.1
   */
  protected function checkbox( $cap, $checked ) {
    printf( '<input type="checkbox" id="%1$s" name="%1$s" value="%1$s" %2$s>' , $cap, $checked );
    printf( '<label for="%1$s">%1$s</label>', $cap );
  }
}
