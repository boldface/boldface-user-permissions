<?php
/**
 * Boldface UserPermissions Plugin
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions;

use \Boldface\UserPermissions\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for loading the plugin
 */
class plugin {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
  * Constructor
  *
  * @access public
  * @since 0.1
  */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add action to load the plugin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on initiation to initialize the plugin
    \add_action( 'init',
      [ new hooks\init( $this->file ), 'register' ], 5 );
  }
}
