<?php
/**
 * Hooks/load-user-edit.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Hooks;

use \Boldface\UserPermissions\hooks as hooks;
use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class load_user_edit {

  /**
   * Add several actions to load user edit page.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_filter( 'additional_capabilities_display', '__return_false' );

    $user_edit = new methods\load_user_edit();
    $user_edit_update = new methods\load_user_edit_update();

    \add_action( 'edit_user_profile', [ $user_edit, 'edit_user_profile' ] );
    \add_action( 'edit_user_profile_update', [ $user_edit_update, 'user_edit_update' ] );

    \add_action( 'show_user_profile', [ $user_edit, 'edit_user_profile' ] );
    \add_action( 'personal_options_update', [ $user_edit_update, 'user_edit_update' ] );

    \add_action( 'admin_enqueue_scripts', [ new hooks\admin_enqueue_scripts(), 'register' ], 5 );
  }
}
