<?php
/**
 * Hooks/admin_enqueue_scripts
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Hooks;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class admin_enqueue_scripts {

  /**
   * @var array CSS styles
   *
   * @access protected
   * @since 0.1
   */
  protected $styles;

  /**
   * @var array JS
   *
   * @access protected
   * @since 0.1
   */
  protected $scripts;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->styles = [
      'flexbox' => [
        'ver' => '0.1',
      ],
    ];
    $this->scripts = [
      'user-permissions' => [
        'deps' => [ 'jquery' ],
        'ver'  => '0.1',
        'localize' => true,
      ],
    ];
  }

  /**
   * Add several actions to the admin footer.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_action( 'admin_enqueue_scripts',
      [ new methods\admin_enqueue_styles( $this->styles ), 'enqueue_styles' ] );

    \add_action( 'admin_enqueue_scripts',
      [ new methods\admin_enqueue_scripts( $this->scripts ), 'enqueue_scripts' ] );
  }
}
