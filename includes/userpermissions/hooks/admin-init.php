<?php
/**
 * Hooks/admin_init
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Hooks;

use \Boldface\UserPermissions\hooks as hooks;
use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class admin_init {

  /**
   * Add several actions to load the admin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on load-users.php to register hooks required on the users edit page
    \add_action( 'load-users.php', [ new hooks\load_users(), 'register' ] );

    //* Fires on load-user-edit.php to register hooks required on the user edit page
    \add_action( 'load-user-edit.php',
      [ $load_user_edit = new hooks\load_user_edit(), 'register' ] );

    //* Fires on load-profile.php to register hooks required on the user edit page
    \add_action( 'load-profile.php', [ $load_user_edit, 'register' ] );

    //* Fires on ajax request role_change
    \add_action( 'wp_ajax_role_change', [ new methods\ajax(), 'role_change' ] );
  }
}
