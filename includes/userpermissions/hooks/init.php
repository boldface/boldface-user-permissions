<?php
/**
 * Hooks/init
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Hooks;

use \Boldface\UserPermissions\methods as methods;
use \Boldface\UserPermissions\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress initiation
 */
class init {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add action to initialize the plugin
   *
   * @access public
   * @since 0.2
   */
  public function register() {

    //* Fires on admin_init to load hooks only for admin
    \add_action( 'admin_init', [ new hooks\admin_init(), 'register' ] );

    //* Fires on initiation to load plugin text domain
    \add_action( 'init',
      [ new methods\load_textdomain( $this->file ), 'register' ] );
  }
}
