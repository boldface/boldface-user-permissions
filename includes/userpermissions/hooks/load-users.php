<?php
/**
 * Hooks/load-users.php
 *
 * @package Boldface User Permissions
 */
namespace Boldface\UserPermissions\Hooks;

use \Boldface\UserPermissions\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress users page
 */
class load_users {

  /**
   * @var Methods used by the class
   *
   * @access protected
   * @since 0.1
   */
  protected $methods;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->methods = new methods\load_users();
  }

  /**
   * Add filters to modify the load-users.php page
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_filter( 'manage_users_columns',
      [ $this->methods, 'manage_users_columns' ] );

    \add_filter( 'manage_users_custom_column',
      [ $this->methods, 'manage_users_custom_column' ], 10, 3 );
  }
}
