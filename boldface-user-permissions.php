<?php
/**
 * Plugin Name: Boldface User Permissions
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-user-permissions/
 * Description: Per user permissions
 * Version: 0.1
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI: https://www.gnu.org/licenses/gpl-2.0.en.html
 * Domain Path: /languages
 * Text Domain: boldface-user-permissions
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Start bootstraping the plugin
require( dirname( __FILE__ ) . '/includes/bootstrap.php' );
add_action( 'plugins_loaded',
  [ $bootstrap = new boldface_user_permissions( __FILE__ ), 'register' ] );

//* Register activation and deactivation hooks
register_activation_hook( __FILE__ , [ $bootstrap, 'activation' ] );
register_deactivation_hook( __FILE__ , [ $bootstrap, 'deactivation' ] );
